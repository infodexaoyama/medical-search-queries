/****** Object:  Table [dbo].[BasicalData]    Script Date: 2019/08/30 15:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Station](
	[Id] [nvarchar](50) NOT NULL PRIMARY KEY,
	[StationGroupCd] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LineCd] [nvarchar](50) NULL,
	[PrefCd] [nvarchar](50) NULL,
	[PostCd] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[Lon] [nvarchar](50) NULL,
	[Lat] [nvarchar](50) NULL,
	[OpenYmd] [nvarchar](50) NULL,
	[CloseYmd] [nvarchar](50) NULL,
	[Estatus] [nvarchar](50) NULL,
	[Esort] [nvarchar](50) NULL,
)
GO
