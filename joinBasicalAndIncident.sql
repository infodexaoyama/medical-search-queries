(
select incident.StoreNo
	 , basic.Region
     , basic.StoreName
     , basic.StoreNameKana
     , basic.NormalizeStoreName
     , basic.Establisher
     , basic.NormalizeEstablisher
     , basic.CompanyName
     , basic.EstablisherKana
     , basic.CompanyNameKana
     , basic.Administrator
     , basic.AdministratorKana
     , basic.PostalCode
     , basic.Address
     , basic.AddressKana
     , basic.PhoneNumber
     , basic.FAX
     , basic.SubPhoneNumber
     , basic.OpenDay
     , basic.Holiday
     , basic.ExceptionalHoliday
     , basic.WebSite
     , basic.CertifiedPharmacist
     , basic.JobDescription
     , basic.HealthCareSupportPharmacist
     , basic.localCooperation
     , basic.MailAddress
     , basic.MeetingResults
     , basic.LocalCooperationResults
     , basic.ComunicationResults
     , basic.SafetyResults
     , basic.SalesResults
     , basic.Features
     , basic.Pharmacists
     , basic.Patients
     , basic.Stock
     , basic.StockGenerics
     , basic.GeneralUse
	 , incident.SpecifiedDate
	 , incident.DesignationStartDate
	 , incident.DepartmentName
	 , incident.Remarks
	 , incident.AcceptInfo
	 , incident.RegistReason
	 , incident.PostalCode
  from dbo.BasicalData as basic
inner join dbo.IncidentalData as incident
on basic.PhoneNumber <> '' and (basic.NormalizePhoneNumber = incident.NormalizePhoneNo)
)
union
(
select incident.StoreNo
	 , joined.Region
     , joined.StoreName
     , joined.StoreNameKana
     , joined.NormalizeStoreName
     , joined.Establisher
     , joined.NormalizeEstablisher
     , joined.CompanyName
     , joined.EstablisherKana
     , joined.CompanyNameKana
     , joined.Administrator
     , joined.AdministratorKana
     , joined.PostalCode
     , joined.Address
     , joined.AddressKana
     , joined.PhoneNumber
     , joined.FAX
     , joined.SubPhoneNumber
     , joined.OpenDay
     , joined.Holiday
     , joined.ExceptionalHoliday
     , joined.WebSite
     , joined.CertifiedPharmacist
     , joined.JobDescription
     , joined.HealthCareSupportPharmacist
     , joined.localCooperation
     , joined.MailAddress
     , joined.MeetingResults
     , joined.LocalCooperationResults
     , joined.ComunicationResults
     , joined.SafetyResults
     , joined.SalesResults
     , joined.Features
     , joined.Pharmacists
     , joined.Patients
     , joined.Stock
     , joined.StockGenerics
     , joined.GeneralUse
	 , incident.SpecifiedDate
	 , incident.DesignationStartDate
	 , incident.DepartmentName
	 , incident.Remarks
	 , incident.AcceptInfo
	 , incident.RegistReason
	 , incident.PostalCode
 from  (
		select basic.Region
             , basic.StoreName
             , basic.StoreNameKana
             , basic.NormalizeStoreName
             , basic.Establisher
             , basic.NormalizeEstablisher
             , basic.CompanyName
             , basic.EstablisherKana
             , basic.CompanyNameKana
             , basic.Administrator
             , basic.AdministratorKana
             , basic.PostalCode
             , basic.Address
             , basic.AddressKana
             , basic.PhoneNumber
             , basic.FAX
             , basic.SubPhoneNumber
             , basic.OpenDay
             , basic.Holiday
             , basic.ExceptionalHoliday
             , basic.WebSite
             , basic.CertifiedPharmacist
             , basic.JobDescription
             , basic.HealthCareSupportPharmacist
             , basic.localCooperation
             , basic.MailAddress
             , basic.MeetingResults
             , basic.LocalCooperationResults
             , basic.ComunicationResults
             , basic.SafetyResults
             , basic.SalesResults
             , basic.Features
             , basic.Pharmacists
             , basic.Patients
             , basic.Stock
             , basic.StockGenerics
             , basic.GeneralUse
		  from dbo.BasicalData as basic
		left outer join dbo.IncidentalData as incident
		on basic.PhoneNumber <> '' and (basic.NormalizePhoneNumber = incident.NormalizePhoneNo)
		 where incident.StoreName IS NULL) as joined
inner join dbo.IncidentalData as incident
on joined.NormalizeStoreName + joined.NormalizeEstablisher = incident.StoreName + incident.Establisher
)
