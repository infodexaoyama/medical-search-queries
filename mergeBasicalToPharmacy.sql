MERGE INTO dbo.PharmacyData AS A
	USING dbo.BasicalData AS B
	ON
	(
		A.Id= B.Id
	)
	WHEN MATCHED THEN
		UPDATE SET
			[Id]=B.[Id]
			,[Region]=B.[Region]
			,[StoreName]=B.[StoreName]
			,[StoreNameKana]=B.[StoreNameKana]
			,[NormalizeStoreName]=B.[NormalizeStoreName]
			,[Establisher]=B.[Establisher]
			,[NormalizeEstablisher]=B.[NormalizeEstablisher]
			,[CompanyName]=B.[CompanyName]
			,[EstablisherKana]=B.[EstablisherKana]
			,[CompanyNameKana]=B.[CompanyNameKana]
			,[Administrator]=B.[Administrator]
			,[AdministratorKana]=B.[AdministratorKana]
			,[PostalCode]=B.[PostalCode]
			,[Address]=B.[Address]
			,[NormalizeAddress]=B.[NormalizeAddress]
			,[AddressKana]=B.[AddressKana]
			,[PhoneNumber]=B.[PhoneNumber]
			,[FAX]=B.[FAX]
			,[SubPhoneNumber]=B.[SubPhoneNumber]
			,[NormalizePhoneNumber]=B.[NormalizePhoneNumber]
			,[OpenDay]=B.[OpenDay]
			,[Holiday]=B.[Holiday]
			,[OpenInWeek]=B.[OpenInWeek]
			,[ExceptionalHoliday]=B.[ExceptionalHoliday]
			,[WebSite]=B.[WebSite]
			,[CertifiedPharmacist]=B.[CertifiedPharmacist]
			,[JobDescription]=B.[JobDescription]
			,[HealthCareSupportPharmacist]=B.[HealthCareSupportPharmacist]
			,[LocalCooperation]=B.[LocalCooperation]
			,[MailAddress]=B.[MailAddress]
			,[MeetingResults]=B.[MeetingResults]
			,[LocalCooperationResults]=B.[LocalCooperationResults]
			,[CommunicationResults]=B.[CommunicationResults]
			,[SafetyResults]=B.[SafetyResults]
			,[SalesResults]=B.[SalesResults]
			,[Features]=B.[Features]
			,[Pharmacists]=B.[Pharmacists]
			,[Patients]=B.[Patients]
			,[Stock]=B.[Stock]
			,[StockGenerics]=B.[StockGenerics]
			,[GeneralUse]=B.[GeneralUse]
			,[Access]=B.[Access]
			,[IsHealthcareSupport]=B.[IsHealthcareSupport]
			,[IsComputerieDrugManagement] = B.[IsComputerieDrugManagement]

	WHEN NOT MATCHED THEN
		INSERT ([Id],[Region],[StoreName],[StoreNameKana],[NormalizeStoreName],[Establisher],[NormalizeEstablisher],[CompanyName],[EstablisherKana],[CompanyNameKana],[Administrator],[AdministratorKana],[PostalCode],[Address],[NormalizeAddress],[AddressKana],[PhoneNumber],[FAX],[SubPhoneNumber],[NormalizePhoneNumber],[OpenDay],[Holiday],[OpenInWeek],[ExceptionalHoliday],[WebSite],[CertifiedPharmacist],[JobDescription],[HealthCareSupportPharmacist],[LocalCooperation],[MailAddress],[MeetingResults],[LocalCooperationResults],[CommunicationResults],[SafetyResults],[SalesResults],[Features],[Pharmacists],[Patients],[Stock],[StockGenerics],[GeneralUse],[Access],[IsHealthcareSupport], [IsComputerieDrugManagement])
		VALUES
		(	
			B.[Id]
			, B.[Region]
			, B.[StoreName]
			, B.[StoreNameKana]
			, B.[NormalizeStoreName]
			, B.[Establisher]
			, B.[NormalizeEstablisher]
			, B.[CompanyName]
			, B.[EstablisherKana]
			, B.[CompanyNameKana]
			, B.[Administrator]
			, B.[AdministratorKana]
			, B.[PostalCode]
			, B.[Address]
			, B.[NormalizeAddress]
			, B.[AddressKana]
			, B.[PhoneNumber]
			, B.[FAX]
			, B.[SubPhoneNumber]
			, B.[NormalizePhoneNumber]
			, B.[OpenDay]
			, B.[Holiday]
			, B.[OpenInWeek]
			, B.[ExceptionalHoliday]
			, B.[WebSite]
			, B.[CertifiedPharmacist]
			, B.[JobDescription]
			, B.[HealthCareSupportPharmacist]
			, B.[LocalCooperation]
			, B.[MailAddress]
			, B.[MeetingResults]
			, B.[LocalCooperationResults]
			, B.[CommunicationResults]
			, B.[SafetyResults]
			, B.[SalesResults]
			, B.[Features]
			, B.[Pharmacists]
			, B.[Patients]
			, B.[Stock]
			, B.[StockGenerics]
			, B.[GeneralUse]
			, B.[Access]
			, B.[IsHealthcareSupport]
			, B.[IsComputerieDrugManagement]
		)
;