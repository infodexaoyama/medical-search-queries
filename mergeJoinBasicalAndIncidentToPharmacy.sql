MERGE INTO dbo.PharmacyData AS P
	USING ((
select incident.StoreNo
	 , basic.Region
     , basic.StoreName
     , basic.StoreNameKana
     , basic.NormalizeStoreName
     , basic.Establisher
     , basic.NormalizeEstablisher
     , basic.CompanyName
     , basic.EstablisherKana
     , basic.CompanyNameKana
     , basic.Administrator
     , basic.AdministratorKana
     , basic.PostalCode
     , basic.Address
     , basic.AddressKana
     , basic.PhoneNumber
     , basic.FAX
     , basic.SubPhoneNumber
     , basic.OpenDay
     , basic.Holiday
	 , basic.OpenInWeek
     , basic.ExceptionalHoliday
     , basic.WebSite
     , basic.CertifiedPharmacist
     , basic.JobDescription
     , basic.HealthCareSupportPharmacist
     , basic.localCooperation
     , basic.MailAddress
     , basic.MeetingResults
     , basic.LocalCooperationResults
     , basic.CommunicationResults
     , basic.SafetyResults
     , basic.SalesResults
     , basic.Features
     , basic.Pharmacists
     , basic.Patients
     , basic.Stock
     , basic.StockGenerics
     , basic.GeneralUse
	 , incident.SpecifiedDate
	 , incident.DesignationStartDate
	 , incident.DepartmentName
	 , incident.Remarks
	 , incident.AcceptInfo
	 , incident.RegistReason
	 , basic.IsHealthcareSupport
  from dbo.BasicalData as basic
inner join dbo.IncidentalData as incident
on basic.PhoneNumber <> '' and (basic.NormalizePhoneNumber = incident.NormalizePhoneNo)
)
union
(
select incident.StoreNo
	 , joined.Region
     , joined.StoreName
     , joined.StoreNameKana
     , joined.NormalizeStoreName
     , joined.Establisher
     , joined.NormalizeEstablisher
     , joined.CompanyName
     , joined.EstablisherKana
     , joined.CompanyNameKana
     , joined.Administrator
     , joined.AdministratorKana
     , joined.PostalCode
     , joined.Address
     , joined.AddressKana
     , joined.PhoneNumber
     , joined.FAX
     , joined.SubPhoneNumber
     , joined.OpenDay
     , joined.Holiday
	 , joined.OpenInWeek
     , joined.ExceptionalHoliday
     , joined.WebSite
     , joined.CertifiedPharmacist
     , joined.JobDescription
     , joined.HealthCareSupportPharmacist
     , joined.localCooperation
     , joined.MailAddress
     , joined.MeetingResults
     , joined.LocalCooperationResults
     , joined.CommunicationResults
     , joined.SafetyResults
     , joined.SalesResults
     , joined.Features
     , joined.Pharmacists
     , joined.Patients
     , joined.Stock
     , joined.StockGenerics
     , joined.GeneralUse
	 , incident.SpecifiedDate
	 , incident.DesignationStartDate
	 , incident.DepartmentName
	 , incident.Remarks
	 , incident.AcceptInfo
	 , incident.RegistReason
	 , joined.IsHealthcareSupport
 from  (
		select basic.Region
             , basic.StoreName
             , basic.StoreNameKana
             , basic.NormalizeStoreName
             , basic.Establisher
             , basic.NormalizeEstablisher
             , basic.CompanyName
             , basic.EstablisherKana
             , basic.CompanyNameKana
             , basic.Administrator
             , basic.AdministratorKana
             , basic.PostalCode
             , basic.Address
             , basic.AddressKana
             , basic.PhoneNumber
             , basic.FAX
             , basic.SubPhoneNumber
             , basic.OpenDay
             , basic.Holiday
             , basic.OpenInWeek
             , basic.ExceptionalHoliday
             , basic.WebSite
             , basic.CertifiedPharmacist
             , basic.JobDescription
             , basic.HealthCareSupportPharmacist
             , basic.localCooperation
             , basic.MailAddress
             , basic.MeetingResults
             , basic.LocalCooperationResults
             , basic.CommunicationResults
             , basic.SafetyResults
             , basic.SalesResults
             , basic.Features
             , basic.Pharmacists
             , basic.Patients
             , basic.Stock
             , basic.StockGenerics
             , basic.GeneralUse
             , basic.IsHealthcareSupport
		  from dbo.BasicalData as basic
		left outer join dbo.IncidentalData as incident
		on basic.PhoneNumber <> '' and (basic.NormalizePhoneNumber = incident.NormalizePhoneNo)
		 where incident.StoreName IS NULL) as joined
inner join dbo.IncidentalData as incident
on joined.NormalizeStoreName + joined.NormalizeEstablisher = incident.StoreName + incident.Establisher
)
) AS J
ON
(
P.StoreNo = J.StoreNo
)
WHEN MATCHED THEN
		UPDATE SET
			StoreNo = J.StoreNo
			, Region = J.Region
			, StoreName = J.StoreName
			, StoreNameKana = J.StoreNameKana
			, NormalizeStoreName = J.NormalizeStoreName
			, Establisher = J.Establisher
			, NormalizeEstablisher = J.NormalizeEstablisher
			, CompanyName = J.CompanyName
			, EstablisherKana = J.EstablisherKana
			, CompanyNameKana = J.CompanyNameKana
			, Administrator = J.Administrator
			, AdministratorKana = J.AdministratorKana
			, PostalCode = J.PostalCode
			, Address = J.Address
			, AddressKana = J.AddressKana
			, PhoneNumber = J.PhoneNumber
			, FAX = J.FAX
			, SubPhoneNumber = J.SubPhoneNumber
			, OpenDay = J.OpenDay
			, Holiday = J.Holiday
			, OpenInWeek = J.OpenInWeek
			, ExceptionalHoliday = J.ExceptionalHoliday
			, WebSite = J.WebSite
			, CertifiedPharmacist = J.CertifiedPharmacist
			, JobDescription = J.JobDescription
			, HealthCareSupportPharmacist = J.HealthCareSupportPharmacist
			, localCooperation = J.localCooperation
			, MailAddress = J.MailAddress
			, MeetingResults = J.MeetingResults
			, LocalCooperationResults = J.LocalCooperationResults
			, CommunicationResults = J.CommunicationResults
			, SafetyResults = J.SafetyResults
			, SalesResults = J.SalesResults
			, Features = J.Features
			, Pharmacists = J.Pharmacists
			, Patients = J.Patients
			, Stock = J.Stock
			, StockGenerics = J.StockGenerics
			, GeneralUse = J.GeneralUse
			, SpecifiedDate = J.SpecifiedDate
			, DesignationStartDate = J.DesignationStartDate
			, DepartmentName = J.DepartmentName
			, Remarks = J.Remarks
			, AcceptInfo = J.AcceptInfo
			, RegistReason = J.RegistReason
			, IsHealthcareSupport = J.IsHealthcareSupport
	WHEN NOT MATCHED THEN
		INSERT (StoreNo, Region, StoreName, StoreNameKana, NormalizeStoreName, Establisher, NormalizeEstablisher, CompanyName, EstablisherKana, CompanyNameKana, Administrator, AdministratorKana, PostalCode, Address, AddressKana, PhoneNumber, FAX, SubPhoneNumber, OpenDay, Holiday, OpenInWeek, ExceptionalHoliday, WebSite, CertifiedPharmacist, JobDescription, HealthCareSupportPharmacist, localCooperation, MailAddress, MeetingResults, LocalCooperationResults, CommunicationResults, SafetyResults, SalesResults, Features, Pharmacists, Patients, Stock, StockGenerics, GeneralUse, SpecifiedDate, DesignationStartDate, DepartmentName, Remarks, AcceptInfo, RegistReason, IsHealthcareSupport)
		VALUES
		(	
			J.StoreNo
			, J.Region
			, J.StoreName
			, J.StoreNameKana
			, J.NormalizeStoreName
			, J.Establisher
			, J.NormalizeEstablisher
			, J.CompanyName
			, J.EstablisherKana
			, J.CompanyNameKana
			, J.Administrator
			, J.AdministratorKana
			, J.PostalCode
			, J.Address
			, J.AddressKana
			, J.PhoneNumber
			, J.FAX
			, J.SubPhoneNumber
			, J.OpenDay
			, J.Holiday
			, J.OpenInWeek
			, J.ExceptionalHoliday
			, J.WebSite
			, J.CertifiedPharmacist
			, J.JobDescription
			, J.HealthCareSupportPharmacist
			, J.localCooperation
			, J.MailAddress
			, J.MeetingResults
			, J.LocalCooperationResults
			, J.CommunicationResults
			, J.SafetyResults
			, J.SalesResults
			, J.Features
			, J.Pharmacists
			, J.Patients
			, J.Stock
			, J.StockGenerics
			, J.GeneralUse
			, J.SpecifiedDate
			, J.DesignationStartDate
			, J.DepartmentName
			, J.Remarks
			, J.AcceptInfo
			, J.RegistReason
			, J.IsHealthcareSupport
		)
;