SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FacilityStandard](
	[Prefecture] [nvarchar](50) NULL,
	[Division] [nvarchar](50) NULL,
	[StoreNo] [nvarchar](50) NULL,
	[SubStoreNo] [nvarchar](50) NULL,
	[StoreSymbol] [nvarchar](50) NULL,
	[StoreName] [nvarchar](255) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[NormalizeAddress] [nvarchar](255) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[NormalizePhoneNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[BedsNum] [nvarchar](50) NULL,
	[BasicDispensingFee] [nvarchar](50) NULL,
	[AdditionOfGenericDrugs] [nvarchar](50) NULL,
	[HasFamilyPharmacist] [bit] NOT NULL,
	[IsVisitTeaching] [bit] NOT NULL,
	[IsDispensingAtHome] [bit] NOT NULL,
	[IsRegionalSupport] [bit] NOT NULL,
	[IsAsepticPreparation] [bit] NOT NULL
) ON [PRIMARY]
GO


