/****** Object:  Table [dbo].[BasicalData]    Script Date: 2019/08/30 15:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TrainCompany](
	[Id] [nvarchar](50) NOT NULL PRIMARY KEY,
	[RrCd] [nvarchar](50) NULL,
	[CompnayName] [nvarchar](50) NOT NULL,
	[CompnayNameKana] [nvarchar](50) NULL,
	[CompnayNameH] [nvarchar](50) NULL,
	[CompanyNameR] [nvarchar](50) NULL,
	[CompnayUrl] [nvarchar](50) NULL,
	[CompanyType] [nvarchar](50) NULL,
	[Estatus] [nvarchar](50) NULL,
	[Esort] [nvarchar](50) NULL,
)
GO
