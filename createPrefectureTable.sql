/****** Object:  Table [dbo].[BasicalData]    Script Date: 2019/08/30 15:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Prefecture](
	[PrefectureCd] [nvarchar](50) NOT NULL PRIMARY KEY,
	[Name] [nvarchar](50) NULL,
)
GO
