SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UnlinkedData](
	[Region] [nvarchar](50) NULL,
	[StoreName] [nvarchar](MAX) NULL,
	[NormalizeStoreName] [nvarchar](MAX) NULL,
	[Establisher] [nvarchar](MAX) NULL,
	[NormalizeEstablisher] [nvarchar](MAX) NULL,
	[CompanyName] [nvarchar](MAX) NULL,
	[Administrator] [nvarchar](MAX) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[Address] [nvarchar](MAX) NULL,
	[PhoneNumber] [nvarchar](MAX) NULL,
	[NormalizePhoneNumber] [nvarchar](MAX) NULL,
	[FAX] [nvarchar](MAX) NULL,
	[SubPhoneNumber] [nvarchar](MAX) NULL,
) ON [PRIMARY]
GO


