SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SpecifiedCode](
	[Prefecture] [nvarchar](10) NULL,
	[StoreNo] [nvarchar](50) NULL,
	[StoreName] [nvarchar](255) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[NormalizeAddress] [nvarchar](255) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[NormalizePhoneNo] [nvarchar](50) NULL,
	[Pharmacist] [nvarchar](255) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Establisher] [nvarchar](255) NULL,
	[Administrator] [nvarchar](50) NULL,
	[SpecifiedDate] [nvarchar](50) NULL,
	[RegistReason] [nvarchar](50) NULL,
	[DesignationStart] [nvarchar](50) NULL,
	[DepartmentName] [nvarchar](255) NULL,
	[Remarks] [nvarchar](255) NULL
) ON [PRIMARY]
GO


