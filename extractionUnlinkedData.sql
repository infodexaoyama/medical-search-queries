INSERT INTO dbo.UnlinkedData(
	Region,
	StoreName,
	NormalizeStoreName,
	Establisher,
	NormalizeEstablisher,
	CompanyName,
	Administrator,
	PostalCode,
	Address,
	PhoneNumber,
	NormalizePhoneNumber,
	FAX,
	SubPhoneNumber
)
select joined.Region
     , joined.StoreName
     , joined.NormalizeStoreName
     , joined.Establisher
     , joined.NormalizeEstablisher
     , joined.CompanyName
     , joined.Administrator
     , joined.PostalCode
     , joined.Address
     , joined.PhoneNumber
	 , joined.NormalizePhoneNumber
     , joined.FAX
     , joined.SubPhoneNumber
 from  (
		select basic.Region
             , basic.StoreName
             , basic.StoreNameKana
             , basic.NormalizeStoreName
             , basic.Establisher
             , basic.NormalizeEstablisher
             , basic.CompanyName
             , basic.EstablisherKana
             , basic.CompanyNameKana
             , basic.Administrator
             , basic.AdministratorKana
             , basic.PostalCode
             , basic.Address
             , basic.AddressKana
             , basic.PhoneNumber
			 , basic.NormalizePhoneNumber
             , basic.FAX
             , basic.SubPhoneNumber
		  from dbo.BasicalData as basic
		left outer join dbo.IncidentalData as incident
		on basic.PhoneNumber <> '' and (basic.NormalizePhoneNumber = incident.NormalizePhoneNo)
		 where incident.StoreName IS NULL) as joined
left outer join dbo.IncidentalData as incident
on joined.NormalizeStoreName + joined.NormalizeEstablisher = incident.StoreName + incident.Establisher
where incident.Pharmacist IS NULL;
