select B.StoreName
,B.NormalizeStoreName
,B.PhoneNumber
,B.NormalizePhoneNumber
,B.Address
,B.NormalizeAddress
,B.Establisher
,B.NormalizeEstablisher
,B.CompanyName
,I.StoreNo
into TWO_StoreName_PhoneNo
from (select * from ONE_Address_PhoneNo where StoreNo IS NULL) as B
left outer join IncidentalData as I
on B.NormalizeStoreName + B.NormalizePhoneNumber = I.StoreName + I.NormalizePhoneNo