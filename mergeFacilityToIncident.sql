MERGE INTO dbo.IncidentalData AS A
	USING dbo.FacilityStandard AS B
	ON
	(
		A.StoreNo = B.StoreNo
	)
	WHEN MATCHED THEN
		UPDATE SET
			StoreSymbol = B.StoreSymbol
			, FaxNo = B.FaxNo
			, BasicDispensingFee = B.BasicDispensingFee
			, AdditionOfGenericDrugs = B.AdditionOfGenericDrugs
			, IsVisitTeaching = B.IsVisitTeaching
			, IsDispensingAtHome = B.IsDispensingAtHome
			, IsRegionalSupport = B.IsRegionalSupport
			, IsAsepticPreparation = B.IsAsepticPreparation
			, Prefecture = B.Prefecture
			, NormalizePhoneNo = B.NormalizePhoneNo
	WHEN NOT MATCHED THEN
		INSERT (StoreNo, StoreName, StoreSymbol, PostalCode, Address, NormalizeAddress, PhoneNo, NormalizePhoneNo, FaxNo, BasicDispensingFee, AdditionOfGenericDrugs, IsVisitTeaching, IsDispensingAtHome, IsRegionalSupport, IsAsepticPreparation, Prefecture)
		VALUES
		(	
			B.StoreNo
			,B.StoreName
			,B.StoreSymbol
			,B.PostalCode
			,B.Address
			,B.NormalizeAddress
			,B.PhoneNo
			,B.NormalizePhoneNo
			,B.FaxNo
			,B.BasicDispensingFee
			,B.AdditionOfGenericDrugs
			,B.IsVisitTeaching
			,B.IsDispensingAtHome
			,B.IsRegionalSupport
			,B.IsAsepticPreparation
			,B.Prefecture
		)
;