MERGE INTO dbo.BasicalData AS A
	USING dbo.UnlinkedData AS B
	ON
	(
		A.Address = B.Address
	)
	WHEN MATCHED THEN
		UPDATE SET
			Region = B.Region
			, StoreName = B.StoreName
			, NormalizeStoreName = B.NormalizestoreName
			, Establisher = B.Establisher
			, NormalizeEstablisher = B.NormalizeEstablisher
			, CompanyName = B.CompanyName
			, Administrator = B.Administrator
			, PostalCode = B.PostalCode
			, Address = B.Address
			, PhoneNumber = B.PhoneNumber
			, NormalizePhoneNumber = B.PhoneNumber
			, FAX = B.FAX
			, SubPhoneNumber = B.SubPhoneNumber
;