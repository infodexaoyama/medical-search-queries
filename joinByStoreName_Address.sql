select B.StoreName
,B.NormalizeStoreName
,B.PhoneNumber
,B.NormalizePhoneNumber
,B.Address
,B.NormalizeAddress
,B.Establisher
,B.NormalizeEstablisher
,B.CompanyName
,I.StoreNo
into THREE_StoreName_Address
from (select * from TWO_StoreName_PhoneNo where StoreNo IS NULL) as B
left outer join IncidentalData as I
on B.NormalizeStoreName + B.NormalizeAddress = I.StoreName + I.NormalizeAddress