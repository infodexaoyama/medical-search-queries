/****** Object:  Table [dbo].[BasicalData]    Script Date: 2019/08/30 15:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Line](
	[Id] [nvarchar](50) NOT NULL PRIMARY KEY,
	[CompanyCd] [nvarchar](50) NULL,
	[LineName] [nvarchar](50) NOT NULL,
	[LineNameKana] [nvarchar](50) NULL,
	[LineNameH] [nvarchar](50) NULL,
	[LineColorC] [nvarchar](50) NULL,
	[LineColorT] [nvarchar](50) NULL,
	[LineType] [nvarchar](50) NULL,
	[Lon] [nvarchar](50) NULL,
	[Lat] [nvarchar](50) NULL,
	[Zoom] [nvarchar](50) NULL,
	[Estatus] [nvarchar](50) NULL,
	[Esort] [nvarchar](50) NULL,
)
GO
