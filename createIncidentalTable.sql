SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IncidentalData](
	[StoreNo] [nvarchar](50) NOT NULL PRIMARY KEY,
	[StoreName] [nvarchar](255) NULL,
	[StoreSymbol] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[NormalizeAddress] [nvarchar](255) NULL,
	[PhoneNo] [nvarchar](20) NULL,
	[NormalizePhoneNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](20) NULL,
	[Prefecture] [nvarchar](10) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[Establisher] [nvarchar](255) NULL,
	[Administrator] [nvarchar](50) NULL,
	[Pharmacist] [nvarchar](255) NULL,
	[SpecifiedDate] [nvarchar](50) NULL,
	[DesignationStartDate] [nvarchar](50) NULL,
	[DepartmentName] [nvarchar](50) NULL,
	[Remarks] [nvarchar](255) NULL,
	[BasicDispensingFee] nvarchar(50),
    [AdditionOfGenericDrugs] nvarchar(50),
	[IsVisitTeaching] [bit] NOT NULL,
	[IsDispensingAtHome] [bit] NOT NULL,
	[IsRegionalSupport] [bit] NOT NULL,
	[IsAsepticPreparation] [bit] NOT NULL,
	[RegistReason] [nvarchar](255) NULL,
	[PostalCode] [nvarchar](50) NULL
) ON [PRIMARY]
GO


