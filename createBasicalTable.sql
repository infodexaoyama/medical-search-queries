/****** Object:  Table [dbo].[BasicalData]    Script Date: 2019/08/30 15:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BasicalData](
	[Id] [nvarchar](50) NOT NULL PRIMARY KEY,
	[Region] [nvarchar](50) NULL,
	[StoreName] [nvarchar](max) NULL,
	[StoreNameKana] [nvarchar](max) NULL,
	[NormalizeStoreName] [nvarchar](max) NULL,
	[Establisher] [nvarchar](max) NULL,
	[NormalizeEstablisher] [nvarchar](max) NULL,
	[CompanyName] [nvarchar](max) NULL,
	[EstablisherKana] [nvarchar](max) NULL,
	[CompanyNameKana] [nvarchar](max) NULL,
	[Administrator] [nvarchar](max) NULL,
	[AdministratorKana] [nvarchar](max) NULL,
	[PostalCode] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[NormalizeAddress] [nvarchar](max) NULL,
	[AddressKana] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[FAX] [nvarchar](max) NULL,
	[SubPhoneNumber] [nvarchar](max) NULL,
	[NormalizePhoneNumber] [nvarchar](max) NULL,
	[OpenDay] [nvarchar](max) NULL,
	[Holiday] [nvarchar](max) NULL,
	[OpenInWeek] [int] NOT NULL,
	[ExceptionalHoliday] [nvarchar](max) NULL,
	[WebSite] [nvarchar](max) NULL,
	[CertifiedPharmacist] [int] NOT NULL,
	[JobDescription] [nvarchar](max) NULL,
	[HealthCareSupportPharmacist] [int] NOT NULL,
	[LocalCooperation] [nvarchar](max) NULL,
	[MailAddress] [nvarchar](max) NULL,
	[MeetingResults] [nvarchar](max) NULL,
	[LocalCooperationResults] [nvarchar](max) NULL,
	[CommunicationResults] [int] NOT NULL,
	[SafetyResults] [nvarchar](max) NULL,
	[SalesResults] [nvarchar](max) NULL,
	[Features] [nvarchar](max) NULL,
	[Pharmacists] [real] NOT NULL,
	[Patients] [int] NOT NULL,
	[Stock] [nvarchar](max) NULL,
	[StockGenerics] [nvarchar](max) NULL,
	[GeneralUse] [int] NOT NULL,
	[Access] [nvarchar](max) NULL,
	[IsHealthcareSupport] [bit] NOT NULL,
	[IsComputerieDrugManagement] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


