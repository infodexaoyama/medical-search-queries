select B.StoreName
,B.NormalizeStoreName
,B.PhoneNumber
,B.NormalizePhoneNumber
,B.Address
,B.NormalizeAddress
,B.Establisher
,B.NormalizeEstablisher
,B.CompanyName
,I.StoreNo
into ONE_Address_PhoneNo
from BasicalData as B
left outer join Incidentaldata as I
on B.NormalizeAddress + B.NormalizePhoneNumber = I.NormalizeAddress + I.NormalizePhoneNo