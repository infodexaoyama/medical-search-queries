select NormalizePhoneNo + NormalizeAddress
	 , count(NormalizePhoneNo + NormalizeAddress)
from IncidentalData 
group by NormalizePhoneNo + NormalizeAddress 
having count(NormalizePhoneNo + NormalizeAddress) >= 2
;
select NormalizePhoneNo + StoreName
     , count(NormalizePhoneNo + StoreName)
from IncidentalData 
group by NormalizePhoneNo + StoreName
having count(NormalizePhoneNo + StoreName) >= 2
;
select NormalizeAddress + StoreName
     , count(NormalizeAddress + StoreName)
from IncidentalData 
group by NormalizeAddress + StoreName
having count(NormalizeAddress + StoreName) >= 2
;