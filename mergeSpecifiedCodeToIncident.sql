MERGE INTO dbo.IncidentalData AS A
	USING dbo.SpecifiedCode AS B
	ON
	(
		A.StoreNo = B.StoreNo
	)
	WHEN MATCHED THEN
		UPDATE SET
			StoreNo = B.StoreNo
			, StoreName = B.StoreName
			, Address = B.Address
			, NormalizeAddress = B.NormalizeAddress
			, CompanyName = B.CompanyName
			, Establisher = B.Establisher
			, Administrator = B.Administrator
			, Pharmacist = B.Pharmacist
			, SpecifiedDate = B.SpecifiedDate
			, DesignationStartDate = B.DesignationStart
			, DepartmentName = B.DepartmentName
			, Remarks = B.Remarks
			, RegistReason = B.RegistReason
			, NormalizePhoneNo = B.NormalizePhoneNo
	
;