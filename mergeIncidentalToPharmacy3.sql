MERGE INTO dbo.PharmacyData AS A
	USING dbo.IncidentalData AS B
	ON
	(
		A.NormalizeStoreName + A.NormalizePhoneNumber = B.StoreName + B.NormalizePhoneNo
	)
	WHEN MATCHED THEN
		UPDATE SET
			StoreNo = B.StoreNo
			, A.CompanyName = CASE
								WHEN A.CompanyName IS NOT NULL THEN A.CompanyName
								ELSE B.CompanyName
							END
			, SpecifiedDate = B.SpecifiedDate
			, DesignationStartDate = B.DesignationStartDate
			, DepartmentName = B.DepartmentName
			, Remarks = B.Remarks
			, RegistReason = B.RegistReason
			, BasicDispensingFee = B.BasicDispensingFee
			, AdditionOfGenericDrugs = B.AdditionOfGenericDrugs
			, IsVisitTeaching = B.IsVisitTeaching
			, IsDispensingAtHome = B.IsDispensingAtHome
			, IsRegionalSupport = B.IsRegionalSupport
			, IsAsepticPreparation = B.IsAsepticPreparation
;